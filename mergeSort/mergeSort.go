package mergesort

//import "fmt"

//import "fmt"

func MergeSort(arr *[]int) {
	a := *arr

	cut(arr, 0, len(a)-1)
	Merge(arr,0,3,4,7)

}
func cut(arr *[]int, head int, end int) (head1 int, end1 int) {
	//fmt.Println(*arr, head, end)
	if end-head == 0 {
		head1 = head
		end1 = end
		return
	}
	head1, end1, head2, end2 := 0, (head+end)/2, (head+end)/2+1, end
	leftHead, leftEnd := cut(arr, head1, end1)
	rightHead, rightEnd := cut(arr, head2, end2)
	return Merge(arr, leftHead, leftEnd, rightHead, rightEnd)

}

func Merge(arr *[]int, leftHead int, leftEnd int, rightHead int, rightEnd int) (head1 int, end1 int) {
	//fmt.Println("merge:", *arr, leftHead, leftEnd, rightHead, rightEnd)
	a := *arr
	head1 = leftHead
	end1 = rightEnd
	for leftEnd-leftHead >= 0 && rightEnd-rightHead >= 0 {
		if a[leftHead] < a[rightHead] {
			leftHead++
		} else {
			swap(&a[leftHead], &a[rightHead])
			rightHead++
		}

	}
	// for j := 0; j < len(left); j++ {
	// 	result[i] = left[j]
	// 	i++
	// }
	// for j := 0; j < len(right); j++ {
	// 	result[i] = right[j]
	// 	i++
	// }

	return
}
func swap(number1 *int, number2 *int) {
	//fmt.Println("aaa")
	*number1, *number2 = *number2, *number1
}
