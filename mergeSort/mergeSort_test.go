package mergesort_test

import (
	"errors"


	"testing"
	"udevsLesson/pointerMergeSort/mergeSort"
)

func TestMergeSort(t *testing.T) {

	tests := []struct {
		name    string
		fields  []int
		want    []int
		wantErr bool
	}{
		{"ok", []int{3, 9, 5, 7, 6, 170}, []int{3, 5, 6, 7, 9, 170}, false},
		{"ok", []int{8, 9, 5, 85, 95, 99}, []int{5, 8, 9, 85, 95, 99}, false},
		{"ok", []int{3, 99, 5, 0, 95, 99}, []int{0, 3, 5, 95, 99, 99}, false},
		{"ok", []int{8, 99, 5, 88, 95, 99}, []int{5, 8, 88, 95, 99, 99}, false},
		{"ok", []int{3, 99, 5, 7, 6, 65}, []int{3, 5, 6, 7, 65, 99}, false},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			mergesort.MergeSort(&tt.fields)
			var err error
			

			for i := 0; i < len(tt.fields); i++ {

				if tt.fields[i] != tt.want[i] {
				
					err = errors.New("don't match item in array")
				}
			}

			if (err != nil) != tt.wantErr {
				t.Errorf("MergeSort error = %v, wantErr %v", err, tt.wantErr)
				return
			}

		})
	}
}
