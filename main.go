package main

import (
	"fmt"
	mergesort "udevsLesson/pointerMergeSort/mergeSort"
)

func main() {

	arr := []int{  3,  9,5,7,6,170}

	mergesort.MergeSort(&arr)
	fmt.Println(arr)
}
